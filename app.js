/**
 * Summary (Plantilla app)
 *
 * Description. (Plantilla app)
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
const path = require('path')
const express = require('express')
var cors = require('cors')
const app = express()
const { logger } = require('./services/logger')
const bodyParser = require('body-parser')
const server = require('http').createServer(app)
const io = require('socket.io')(server)
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, './public')))
var request = require('request')
app.use(cors())
/**
 * VARIABLES DE ENTORNO
 */
const IP_DADOS = process.env.IP_DADOS || '34.71.49.153:8000/grupo4/juego'
const IP_USUARIO = process.env.IP_USUARIO || '104.198.172.61:3000'
const IP_TORNEOS = process.env.IP_TORNEOS || 'localhost:7000'
const IP_TOKEN = process.env.IP_TOKEN || '34.71.49.153:5000'
const ID_SECRET = process.env.ID_SECRET || 'parchis'
const SECRET_SECRET = process.env.SECRET_SECRET || 'gjq0JaeBPi'
const ENCRYPT = process.env.ENCRYPT || 'c2E6QWJjMTIzKio='
const partidadGeneradas = {} // IDs partidas que vienen de /generar
// const jugadoresPartidas = [] // IDs jugadores que vienen de /generar
const salasClientes = {} // Socket IO salas {Key:Value} => {IDClient:Partida}
let ultimoId = '' // ultima Sala recibida /:id
const salasActivas = [] // Salas registradas
const estadosPartidas = {}
let bearerToken = ''
let controlLogin = 0
/**
 *
 * Description. (desplega el tablero de una sala)
 *
 * @param {Path-uuid} id   ID de la partida que se disputara
*/
app.get('/unirse/:id', (req, res) => {
  logger.log({
    level: 'info',
    class: 'index',
    message: 'peticion inicial'
  })
  ultimoId = req.params.id
  logger.log({
    level: 'info',
    class: 'index',
    message: `peticion a unirse a la partida ${ultimoId}`
  })
  res.statusCode = 200
  res.sendFile('tablero.html', { root: path.join(__dirname, './public') })
})

/**
 *
 * Description. (ingreso sin autorizacion)
 *
*/
app.get('/error', (req, res) => {
  logger.log({
    level: 'warn',
    class: 'index',
    message: 'redireccion a la pagina de error por falta de autenticacion, violacion de permisos'
  })
  res.statusCode = 200
  res.sendFile('401.html', { root: path.join(__dirname, './public') })
})

/**
 *
 * Description. (seleccion de partida)
 *
*/
app.get('/seleccion', (req, res) => {
  logger.log({
    level: 'info',
    class: 'index',
    message: 'un jugador entro a la pantalla de seleccion de partida'
  })
  res.statusCode = 200
  res.sendFile('seleccion.html', { root: path.join(__dirname, './public') })
})

/**
 *
 * Description. (Generar una partida)
 * @param {Body-uuid} id   id de la partida
 * @param {Body-int[]} jugadores   ids de los jugadores
*/
app.post('/generar', (req, res) => {
  logger.log({
    level: 'info',
    class: 'app',
    message: 'generar una partida'
  })
  const partidaId = req.body.id
  const jugadores = req.body.jugadores
  if (partidaId && jugadores) {
    logger.log({
      level: 'info',
      class: 'app',
      message: `partida creada con id ${partidaId} y jugadores ${jugadores}`
    })
    partidadGeneradas[partidaId] = { jugadores: [jugadores[0], jugadores[1]], l1: false, l2: false }
    // jugadoresPartidas.push()
    res.statusCode = 201
    res.send('partida creada')
  } else {
    logger.log({
      level: 'error',
      class: 'app',
      message: 'parametros no validos en generar partida'
    })
    res.statusCode = 406
    res.end('parametros no validos')
  }
})

/**
 *
 * Description. (Simula una partida)
 * @param {Body-uuid} id   id de la partida
 * @param {Body-int[]} jugadores   ids de los jugadores
*/
app.post('/simular', (req, res) => {
  logger.log({
    level: 'info',
    class: 'app',
    message: 'simula una partida'
  })
  const partidaId = req.body.id
  const jugadores = req.body.jugadores
  if (partidaId && jugadores) {
    logger.log({
      level: 'info',
      class: 'app',
      message: 'inicio a simular partida ' + partidaId + ' y jugadores ' + jugadores
    })

    // const indice = partidadGeneradas.findIndex(busc => busc === partidaId)
    /* if (partidadGeneradas.partidaId) {
      const juga = partidadGeneradas.partidaId
      if (juga[0] === jugadores[0] && juga[1] === jugadores[1]) {
        // jugadoresPartidas.splice(indice, 1)
        // partidadGeneradas.splice(indice, 1)
        delete partidadGeneradas.partidaId */
    simularPartida(partidaId)
    logger.log({
      level: 'info',
      class: 'app',
      message: 'partida simulada con id ' + partidaId + ' y jugadores ' + jugadores
    })
    res.statusCode = 201
    res.send('partida simulada')
    /* }  else {
        logger.log({
          level: 'warn',
          class: 'app',
          message: `partida ${partidaId} no fue simulada porque se recibieron distintos jugadores`
        })
        res.statusCode = 404
        res.send('jugadores distintos')
      } */
    /* }  else {
      logger.log({
        level: 'error',
        class: 'app',
        message: `partida ${partidaId} no fue simulada porque no existe`
      })
      res.statusCode = 404
      res.send('partida no encontrada')
    } */
  } else {
    logger.log({
      level: 'error',
      class: 'app',
      message: 'parametros no validos en simular partida'
    })
    res.statusCode = 406
    res.end('parametros no validos')
  }
})

/**
 *
 * Description. (encargada del manejo de las peticiones de los clientes)
 * @param {function} client   Cliente que se conecta a socketIO
*/
io.on('connection', function (client) {
  console.log(client.id)
  // io.sockets.in(salasClientes[client.id]).emit('solicitarDados', JSON.parse(response.body))
  // añadir salas
  if (ultimoId !== '') {
    if (!salasActivas.includes(ultimoId)) {
      salasClientes[client.id] = ultimoId
      client.join(ultimoId)
      salasActivas.push(ultimoId)
    } else {
      salasClientes[client.id] = ultimoId
      client.join(ultimoId)
      if (!estadosPartidas[ultimoId]) {
        estadosPartidas[ultimoId] = {
          turno: 1,
          j1: ['casaR1', 'casaR2', 'casaR3', 'casaR4'],
          j2: ['casaY1', 'casaY2', 'casaY3', 'casaY4'],
          d1: '',
          d2: '',
          b1: '1',
          b2: '0.5',
          p1: '0',
          p2: '0'
        }
      }
      io.sockets.in(salasClientes[client.id]).emit('iniciar', 'inicio')
    }
    ultimoId = ''
  } else {
    salasClientes[client.id] = 'login' + controlLogin
    controlLogin++
    client.join(salasClientes[client.id])
  }

  client.on('sincronizar', function (data) {
    data = JSON.parse(data)
    estadosPartidas[ultimoId] = {
      turno: data.turno,
      j1: data.j1,
      j2: data.j2,
      d1: data.d1,
      d2: data.d2,
      b1: data.b1,
      b2: data.b2,
      p1: data.p1,
      p2: data.p2
    }
    io.sockets.in(salasClientes[client.id]).emit('sincronizar', estadosPartidas[ultimoId])
  })

  client.on('loginPrincipal', function (data) {
    obtenerToken()
    const nombre = data.usu
    const pass = data.pass
    const urlUsuario = `http://${IP_USUARIO}/login?email=${nombre}&password=${pass}`
    logger.log({
      level: 'info',
      class: 'app',
      message: 'login de jugador ' + nombre + ' al endopoint ' + urlUsuario
    })
    var options = {
      method: 'GET',
      url: urlUsuario,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearerToken}`
      }
    }
    // console.log(options.headers)
    request(options, async (error, response) => {
      if (error) {
        logger.log({
          level: 'error',
          class: 'app',
          message: 'login de jugador ' + nombre + ' al endopoint ' + urlUsuario
        })
      } else {
        try {
          const respuesta = JSON.parse(response.body)
          respuesta.partida = buscarPartida(respuesta.id)
          respuesta.numeroJugador = Number(numeroJugador(respuesta.id))
          respuesta.juegos = await partidasPorJugar(respuesta.id)
          io.sockets.in(salasClientes[client.id]).emit('loginPrincipal', respuesta)
        } catch (error) {
          logger.log({
            level: 'error',
            class: 'app',
            message: 'login de jugador ' + nombre + ' mala credenciales '
          })
          io.sockets.in(salasClientes[client.id]).emit('loginError', 'mal password')
        }
      }
    })
  })

  client.on('solicitarDados', function (data) {
    obtenerToken()
    const urlDados = 'http://' + IP_DADOS + '/tirar/2'
    logger.log({
      level: 'info',
      class: 'appJuego',
      message: 'solicitud de dados para jugador ' + data + ' al endpoint ' + urlDados
    })
    var options = {
      method: 'GET',
      url: urlDados,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearerToken}`
      }
    }
    // console.log(options.headers)
    request(options, function (error, response) {
      if (error) {
        logger.log({
          level: 'error',
          class: 'appJuego',
          message: 'solicitud de dados para jugador ' + data + ' al endpoint ' + urlDados
        })
      } else {
        // client.emit('solicitarDados', JSON.parse(response.body))
        // client.broadcast.emit('solicitarDados', JSON.parse(response.body))
        try {
          io.sockets.in(salasClientes[client.id]).emit('solicitarDados', JSON.parse(response.body))
        } catch (error) {
          logger.log({
            level: 'error',
            class: 'appJuego',
            message: 'response error solicitud dados jugador ' + data + ' al endpoint ' + urlDados
          })
        }
      }
    })
  })

  client.on('gano', function (data) {
    const urlTorneos = `http://${IP_TORNEOS}/partidas/${data.partida}`
    // console.log('urlTorneos ' + urlTorneos)
    // console.log('data ' + data)
    logger.log({
      level: 'info',
      class: 'appJuego',
      message: 'fin de partida ' + data.partida + ' al endpoint ' + urlTorneos
    })
    var options = {
      method: 'PUT',
      url: urlTorneos,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearerToken}`
      },
      body: JSON.stringify({ marcador: data.marcadores })

    }
    request(options, function (error, response) {
      if (error) {
        logger.log({
          level: 'error',
          class: 'appJuego',
          message: `Error al finalizar la partida ${data.partida} en torneo`
        })
      } else {
        logger.log({
          level: 'info',
          class: 'appJuego',
          message: `La partida ${data.partida} en torneo ha finalizado`
        })
        // client.emit('gano', 'finalizado')
        // client.broadcast.emit('gano', 'finalizado')
        try { delete partidadGeneradas[data.partida] } catch (error) { console.log(error + ' en gano') }
        io.sockets.in(salasClientes[client.id]).emit('gano', 'finalizado')
      }
    })
  })

  client.on('disconnect', function (data) {
    const dejar = salasClientes[client.id]
    client.leave(dejar)
    logger.log({
      level: 'warn',
      class: 'app',
      message: 'desconectado ' + client.id
    })
  })
})

/**
 *
 * Description. (encuentra la partida que debe disputar el jugador)
 * @param {int} jugadores   id de los jugadores
 * @returns {uuid} ID de la partida que identifica la sala a la cual conectarse
*/
function buscarPartida (idJugador) {
  for (const idPartida in partidadGeneradas) {
    const objeto = partidadGeneradas[idPartida]
    if (objeto.jugadores.includes(idJugador)) {
      let indice = objeto.jugadores.findIndex(busc => busc === idJugador)
      indice++
      if (indice === 1) {
        if (!objeto.l1) {
          return idPartida
        }
      } else if (indice === 2) {
        if (!objeto.l2) {
          return idPartida
        }
      }
    }
  }
  return -1
}

/**
 *
 * Description. (encuentra el numero de jugador (1 | 2) que tiene dentro de la partida que debe disputar el jugador)
 * @param {int} jugadores   id de los jugadores
 * @returns {int} jugardor (1 | 2)
*/
function numeroJugador (idJugador) {
  for (const idPartida in partidadGeneradas) {
    const objeto = partidadGeneradas[idPartida]
    if (objeto.jugadores.includes(idJugador)) {
      let indice = objeto.jugadores.findIndex(busc => busc === idJugador)
      indice++
      if (indice === 1) {
        if (!objeto.l1) {
          // objeto.l1 = true
          return indice
        }
      } else if (indice === 2) {
        if (!objeto.l2) {
          // objeto.l2 = true
          return indice
        }
      }
    }
  }
  return -1
}

/**
 *
 * Description. (obtiene un token valido para estar realizando consultas)
*/
function obtenerToken () {
  return new Promise((resolve, reject) => {
    const urlToken = `http://${IP_TOKEN}/token?id=${ID_SECRET}&secret=${SECRET_SECRET}`
    /* logger.log({
      level: 'info',
      class: 'appJuego',
      message: `obtencion de token a la url ${urlToken}`
    }) */

    var options = {
      method: 'POST',
      url: urlToken,
      headers: {
        'Content-Type': 'application/json'// ,
        // Authorization: `Basic ${ENCRYPT}`
      }
    }
    request(options, function (error, response) {
      if (error) {
        logger.log({
          level: 'error',
          class: 'appJuego',
          message: `obtencion de token a la url ${urlToken} con error`
        })
      }
      // console.log(response.body)
      const parseo = JSON.parse(response.body)
      bearerToken = parseo.jwt
      logger.log({
        level: 'info',
        class: 'appJuego',
        message: `obtencion de token a la url ${urlToken} fue exitoso`
      })
      resolve(true)
    })
  })
}

/**
 *
 * Description. (simula una partida)
 * @param {uuid} partida   id de la partida
*/
function simularPartida (partida) {
  const urlTorneos = `http://${IP_TORNEOS}/partidas/${partida}`
  logger.log({
    level: 'info',
    class: 'appJuego',
    message: 'fin de partida ' + partida + ' al endpoint ' + urlTorneos
  })

  const marcadores = []
  const num = Math.ceil(Math.random() * 4)
  const num2 = num === 4 ? Math.ceil(Math.random() * 3) : 4
  marcadores.push(num)
  marcadores.push(num2)
  var options = {
    method: 'PUT',
    url: urlTorneos,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${bearerToken}`
    },
    body: JSON.stringify({ marcador: marcadores })

  }
  request(options, function (error, response) {
    if (error) {
      logger.log({
        level: 'error',
        class: 'appJuego',
        message: `Error al finalizar la partida ${partida} en torneo mediante simulacion`
      })
    } else {
      logger.log({
        level: 'info',
        class: 'appJuego',
        message: `La partida ${partida} en torneo ha finalizado mediante simulacion y se envio el resultado a ${urlTorneos}`
      })
    }
  })
}

/**
 *
 * Description. (retorna un array de todos las partidas que tiene el jugador)
 * @param {int} jugadores   id de los jugadores
 * @returns {Array[object]} partidas pendientes por jugar
*/
async function partidasPorJugar (idJugador) {
  const arrayPartidas = []
  for (const idPartida in partidadGeneradas) {
    const objeto = partidadGeneradas[idPartida]
    const auxiliar = []
    const partida = {
      id: idPartida
    }
    if (objeto.jugadores.includes(idJugador)) {
      for (let index = 0; index < objeto.jugadores.length; index++) {
        const nombreOponente = await obtenerNombreJugador(objeto.jugadores[index])
        auxiliar.push(objeto.jugadores[index] + ' ' + nombreOponente)
      }
      partida.jugadores = auxiliar
      arrayPartidas.push(partida)
    }
  }
  return arrayPartidas
}

/**
 *
 * Description. (retorna un array de todos las partidas que tiene el jugador)
 * @param {int} jugadores   id de los jugadores
 * @returns {String} nombre + apellido del jugador
*/
function obtenerNombreJugador (id) {
  return new Promise((resolve, reject) => {
    const urlUsuario = `http://${IP_USUARIO}/jugadores/${id}`
    /* logger.log({
      level: 'info',
      class: 'appJuego',
      message: `obtencion de token a la url ${urlToken}`
    }) */

    var options = {
      method: 'GET',
      url: urlUsuario,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearerToken}`
      }
    }
    request(options, function (error, response) {
      if (error) {
        logger.log({
          level: 'error',
          class: 'app',
          message: `obtener jugador con id ${id}`
        })
      } else {
        try {
          const respuesta = JSON.parse(response.body)
          resolve(respuesta.nombres + ' ' + respuesta.apellidos)
        } catch (error) {
          logger.log({
            level: 'error',
            class: 'app',
            message: `obtener jugador con id ${id}`
          })
          resolve('Desconocido')
        }
      }
    })
  })
}

obtenerToken()
module.exports = server
