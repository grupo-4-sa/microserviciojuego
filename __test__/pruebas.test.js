const request = require('supertest')
const app = require('../app')
let server, agent
const port = process.env.PORT || 4000

// Se inicia el servidor para comenzar a escuchar peticiones
beforeEach(done => {
  server = app.listen(port, err => {
    if (err) return done(err)
    agent = request.agent(server) // se obtiene el puerto en el que corre el server
    done()
  })
})
// despues de cada prueba se cierra el servidor iniciado en beforeEach
afterEach(done => {
  return server && server.close(done)
})

// Server
describe('Pruebas para App/server', function () {
  test('get /', function (done) {
    request(app)
      .get('/')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('get /error', function (done) {
    request(app)
      .get('/error')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('get /seleccion', function (done) {
    request(app)
      .get('/seleccion')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('get /unirse/:id', function (done) {
    request(app)
      .get('/unirse/:id')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('post /generar valido', function (done) {
    request(app)
      .post('/generar')
      .send(
        {
          id: '2',
          jugadores: ['2', '3']
        })
      .expect(201)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('post /generar invalido falta de parametros', function (done) {
    request(app)
      .post('/generar')
      .send(
        {
          id: '',
          jugadores: ['2', '3']
        })
      .expect(406)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('post /simular valido', function (done) {
    request(app)
      .post('/simular')
      .send(
        {
          id: '2',
          jugadores: ['2', '3']
        })
      .expect(201)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })

  test('post /simular invalido falta parametros', function (done) {
    request(app)
      .post('/simular')
      .send(
        {
          id: '',
          jugadores: ['2', '3']
        })
      .expect(406)
      .end(function (err, res) {
        if (err) return done(err)
        done()
      })
  })
})
