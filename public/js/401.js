if (!sessionStorage.getItem('partida')) {
  let url = window.location.href
  url = url.split('/')
  let path = 'http://'
  for (let index = 0; index < url.length; index++) {
    const element = url[index]
    if (element.includes('http') || element === '' || element.includes('unirse') || element.includes('seleccion')) {
      continue
    }
    if ((element.includes('.') || element.includes(':'))) {
      path += element
      path += '/'
      continue
    }

    if (index < url.length - 2) {
      path += element
      path += '/'
    }
  }
  window.location.replace(`${path}error`)
}
