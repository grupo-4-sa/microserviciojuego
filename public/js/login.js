const signUpButton = document.getElementById('signUp')
const signInButton = document.getElementById('signIn')
const container = document.getElementById('container')
const loginButton = document.getElementById('loginb')
const textoUsuario = document.getElementById('ussinput')
const textoPass = document.getElementById('passinput')
const socket = io.connect()
signUpButton.addEventListener('click', () => {
  container.classList.add('right-panel-active')
})

signInButton.addEventListener('click', () => {
  container.classList.remove('right-panel-active')
})

loginButton.addEventListener('click', (e) => {
  e.preventDefault()
  var txtuss = textoUsuario.value
  var txtpass = textoPass.value
  socket.emit('loginPrincipal', { usu: txtuss, pass: txtpass })
})

socket.on('loginError', function (data) {
  alert('Credenciales erroneas')
})

socket.on('loginPrincipal', function (data) {
  /*
          {
            "id": 0,
            "nombres": "string",
            "apellidos": "string",
            "administrador": true
          }
        */
  if (data.partida) {
    sessionStorage.setItem('id', data.id)
    sessionStorage.setItem('nombre', data.nombres)
    sessionStorage.setItem('apellidos', data.apellidos)
    sessionStorage.setItem('numeroJugador', data.numeroJugador)
    sessionStorage.setItem('partida', data.partida)
    sessionStorage.setItem('juegos', JSON.stringify(data.juegos))
    let url = window.location.href
    url = url.substr(0, url.length - 1)
    window.location.replace(`${url}/seleccion`)
  } else {
    alert('usted no tiene partidas pendientes')
  }
  console.log(data)
})
