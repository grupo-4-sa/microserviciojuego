// server.js
const app = require('./app')
const { logger } = require('./services/logger')
const port = process.env.PORT || 4000

app.listen(port, () => {
  logger.log({
    level: 'info',
    class: 'index',
    message: `Servidor Iniciado en el puerto ${port}`
  })
})
