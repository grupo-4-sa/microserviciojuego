const socket = io.connect()
const bTirar = document.getElementById('bTirar') // boton tirar jugador 1
const bTirar2 = document.getElementById('bTirar2') // boton tirar jugador 2
const idenTurno = document.getElementById('idenTurno') // numero que identifica el jugador titulo
const jugador1 = document.getElementById('jugador1') // nombre jugador 1
const jugador2 = document.getElementById('jugador2') // nombre jugador 2
const puntuacion1 = document.getElementById('puntuacion1') // puntuacion jugador 1
const puntuacion2 = document.getElementById('puntuacion2') // puntuacion jugador 2
const dado1 = document.getElementById('dado1') // dado mostrado en tabla siguiente movimiento
const dado2 = document.getElementById('dado2') // dado mostrado en tabla de ultimo
const caminoJugador1 = ['cass18', 'cass14'] // marca inicio y final del jugador 1
const caminoJugador2 = ['cass44', 'cass40'] // marca inicio y final del jugador 2
let fichasJugador1 = ['', '', '', ''] // fichas de jugador
let fichasJugador2 = ['', '', '', ''] // fichas de jugador
let turno = 1
let pulsadoTurno1 = false
let pulsadoTurno2 = false
let iniciado = false

socket.on('iniciar', function (data) {
  iniciado = true
})

socket.on('sincronizar', function (data) {
  borrarEventos()
  fichasJugador1.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = ''
      ele.style.backgroundSize = ''
    }
  })

  fichasJugador2.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = ''
      ele.style.backgroundSize = ''
    }
  })

  turno = data.turno
  setTurno(turno)
  fichasJugador1 = data.j1
  fichasJugador2 = data.j2
  dado1.innerText = data.d1
  dado2.innerText = data.d2
  bTirar.opacity = data.b1
  bTirar2.opacity = data.b2
  puntuacion1.innerText = data.p1
  puntuacion2.innerText = data.p2
  fichasJugador1.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = 'url("../img/tokenRed.png")'
      ele.style.backgroundSize = (ele.style.backgroundSize === '') ? '100% 100%' : '50% 100%'
    }
  })

  fichasJugador2.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = 'url("../img/tokenAma.png")'
      ele.style.backgroundSize = (ele.style.backgroundSize === '') ? '100% 100%' : '50% 100%'
    }
  })

  eventosFichas()
})

socket.on('jugador1', function (data) {
  jugador1.innerText = data
})

socket.on('jugador2', function (data) {
  jugador2.innerText = data
})

socket.on('gano', function (data) {
  alert('¡FELICIDADES GANASTE! ')
})

function colocarFicha (posFinal, indiceArray) {
  if (indiceArray !== -1) {
    if (turno === 1) {
      fichasJugador1[indiceArray] = posFinal
    } else {
      fichasJugador2[indiceArray] = posFinal
    }
  }
}

function sacarFichas (d1, d2) {
  let contadorCasa = 0
  let numFicha = -1
  if (turno === 1) {
    let valido = 0
    for (let index = 0; index < fichasJugador1.length; index++) {
      const element = fichasJugador1[index]
      if (!element.includes('casa')) {
        valido++
      }
    }

    if (valido !== 4) {
      fichasJugador1.forEach(element => {
        if (element === caminoJugador1[0]) {
          contadorCasa++
        }
      })
      // Mientras mi casilla inicial no tenga 2 fichas, yo puedo sacar fichas
      if (contadorCasa !== 2) {
        if (d2 === 5) {
          for (let index = 0; index < fichasJugador1.length; index++) {
            const element = fichasJugador1[index]
            if (element === ('casaR' + String(index + 1))) {
              numFicha = index
              document.getElementById(element).style.backgroundImage = ''
              break
            }
          }
          colocarFicha(caminoJugador1[0], numFicha)
          dado2.innerText = ''
          d2 = -1
          contadorCasa++
          // saco 2 fichas y existe una del jugador contrario me la como
          if (fichasJugador2.includes(caminoJugador1[0])) {
            const fichaComida = fichasJugador2.findIndex(buscar => buscar === caminoJugador1[0])
            fichasJugador2[fichaComida] = 'casaY1'
            dado2.innerText = '20'
            d2 = 20
          }
        }
        if (d1 === 5 && contadorCasa < 2) {
          for (let index = 0; index < fichasJugador1.length; index++) {
            const element = fichasJugador1[index]
            if (element === ('casaR' + String(index + 1))) {
              numFicha = index
              document.getElementById(element).style.backgroundImage = ''
              break
            }
          }
          colocarFicha(caminoJugador1[0], numFicha)
          dado1.innerText = ''
          d1 = -1
          contadorCasa++
          // saco 2 fichas y existe una del jugador contrario me la como
          if (fichasJugador2.includes(caminoJugador1[0])) {
            const fichaComida = fichasJugador2.findIndex(buscar => buscar === caminoJugador1[0])
            fichasJugador2[fichaComida] = 'casaY1'
            dado1.innerText = '20'
            d1 = 20
          }
          // roto dados
          if (d1 === -1 && d2 !== -1) {
            dado1.innerText = dado2.innerText
            d1 = Number(dado1.innerText)
            d2 = -1
            dado2.innerText = ''
          }
        } else {
          // roto dados
          if (d1 === -1 && d2 !== -1) {
            dado1.innerText = dado2.innerText
            d1 = Number(dado1.innerText)
            d2 = -1
            dado2.innerText = ''
          }
        }
      }
      // realice 2 movimientos al tirar dados, doble 5 y no comi un enemigo
      /* if (d1 === -1 && d2 === -1) {
        // setTurno(2)
      } else {
        if (fichasJugador1.findIndex(buscar => buscar === caminoJugador1[0]) !== -1) {
          colocarEvento(document.getElementById(caminoJugador1[0]))
        }
      } */
      pintarTablero()
      // bloqueo en casa jugador 1
      if (contadorCasa === 2) {
        document.getElementById(caminoJugador1[0]).style.backgroundSize = '50% 100%'
      }
    }
  } else {
    // jugador 2
    let valido = 0
    for (let index = 0; index < fichasJugador2.length; index++) {
      const element = fichasJugador2[index]
      if (!element.includes('casa')) {
        valido++
      }
    }
    if (valido !== 4) {
      fichasJugador2.forEach(element => {
        if (element === caminoJugador2[0]) {
          contadorCasa++
        }
      })
      // Mientras mi casilla inicial no tenga 2 fichas, yo puedo sacar fichas
      if (contadorCasa !== 2) {
        if (d2 === 5) {
          for (let index = 0; index < fichasJugador2.length; index++) {
            const element = fichasJugador2[index]
            if (element === ('casaY' + String(index + 1))) {
              numFicha = index
              document.getElementById(element).style.backgroundImage = ''
              break
            }
          }
          colocarFicha(caminoJugador2[0], numFicha)
          dado2.innerText = ''
          d2 = -1
          contadorCasa++
          // saco 2 fichas y existe una del jugador contrario me la como
          if (fichasJugador1.includes(caminoJugador2[0])) {
            const fichaComida = fichasJugador1.findIndex(buscar => buscar === caminoJugador2[0])
            fichasJugador1[fichaComida] = 'casaR1'
            dado2.innerText = '20'
            d2 = 20
          }
        }
        if (d1 === 5 && contadorCasa < 2) {
          for (let index = 0; index < fichasJugador2.length; index++) {
            const element = fichasJugador2[index]
            if (element === ('casaY' + String(index + 1))) {
              numFicha = index
              document.getElementById(element).style.backgroundImage = ''
              break
            }
          }
          colocarFicha(caminoJugador2[0], numFicha)
          dado1.innerText = ''
          d1 = -1
          contadorCasa++
          // saco 2 fichas y existe una del jugador contrario me la como
          if (fichasJugador1.includes(caminoJugador2[0])) {
            const fichaComida = fichasJugador1.findIndex(buscar => buscar === caminoJugador2[0])
            fichasJugador1[fichaComida] = 'casaR1'
            dado1.innerText = '20'
            d1 = 20
          }
          // roto dados
          if (d1 === -1 && d2 !== -1) {
            dado1.innerText = dado2.innerText
            d1 = Number(dado1.innerText)
            d2 = -1
            dado2.innerText = ''
          }
        } else {
          // roto dados
          if (d1 === -1 && d2 !== -1) {
            dado1.innerText = dado2.innerText
            d1 = Number(dado1.innerText)
            d2 = -1
            dado2.innerText = ''
          }
        }
      }
      // realice 2 movimientos al tirar dados, doble 5 y no comi un enemigo
      /* if (d1 === -1 && d2 === -1) {
        // setTurno(1)
      } else {
        if (fichasJugador2.findIndex(buscar => buscar === caminoJugador2[0]) !== -1) {
          colocarEvento(document.getElementById(caminoJugador2[0]))
        }
      } */
      pintarTablero()
      // bloqueo en casa jugador 2
      if (contadorCasa === 2) {
        document.getElementById(caminoJugador2[0]).style.backgroundSize = '50% 100%'
      }
    }
  }
}

function bloqueosJugador (fichas) {
  const bloqueos = []
  for (let index = 0; index < fichas.length; index++) {
    const element = fichas[index]
    for (let index1 = index + 1; index1 < fichas.length; index1++) {
      const element1 = fichas[index1]
      if (element.includes(element1)) {
        bloqueos.push(Number(element1.substr(4, 2)))
        continue
      }
    }
  }
  return bloqueos
}

// TODO: bloqueo en casilla cercanas al 1, o bien sobre pasar bloqueo con movimiento 20
// true = pase sobre un bloqueo
function revisarBloqueo (bloqueos, inicio, fin) {
  for (let index = 0; index < bloqueos.length; index++) {
    const element = bloqueos[index]
    if ((inicio < element && fin > element) || fin === element) {
      return true
    }
  }
  return false
}

// true = movimiento valido
function movimientoValido (inicio, fin) {
  const bloqueos = turno === 1 ? bloqueosJugador(fichasJugador2) : bloqueosJugador(fichasJugador1)
  // const final = turno === 1 ? Number(caminoJugador1[1].substr(4, 2)) : Number(caminoJugador2[1].substr(4, 2))
  if (inicio.includes('casa') || inicio.includes('fin')) {
    return false
  } else if (inicio.includes('cassR') || inicio.includes('cassY')) {
    const casillaIni = Number(inicio.substr(5, 2))
    const casillaFin = casillaIni + fin
    if (casillaFin > 6) {
      return false
    }
  } else {
    const casillaIni = Number(inicio.substr(4, 2))
    let casillaFin = Number(casillaIni) + fin
    casillaFin = casillaFin > 52 ? casillaFin - 52 : casillaFin
    // no puedo pasar por bloqueo
    if (revisarBloqueo(bloqueos, casillaIni, casillaFin)) {
      return false
    }

    if (fin === 20) {
      if (turno === 2 && ((Number(casillaIni) >= 27 && Number(casillaIni) <= 39) || Number(casillaIni) === 26)) {
        return false
      }
      if (turno === 1 && ((Number(casillaIni) >= 1 && Number(casillaIni) <= 13) || Number(casillaIni) === 52)) {
        return false
      }
    }
    // meterlo a la casa
    /* if (final < casillaFin && inicio < final) {
      return true
    } */
  }
  return true
}
// movimientos disponibles = true
function movimientosValidos () {
  if (dado1.innerText !== '') {
    const d1 = Number(dado1.innerText)
    if (turno === 1) {
      for (let index = 0; index < fichasJugador1.length; index++) {
        const ele = fichasJugador1[index]
        if (movimientoValido(ele, d1)) {
          return true
        }
      }
    } else {
      for (let index = 0; index < fichasJugador2.length; index++) {
        const ele = fichasJugador2[index]
        if (movimientoValido(ele, d1)) {
          return true
        }
      }
    }
  }
  return false
}

function borrarEventos () {
  // borro eventos no pulsados
  fichasJugador1.forEach(element => {
    if (element !== 'fin') {
      const ele = document.getElementById(element)
      const elClone = ele.cloneNode(true)
      ele.parentNode.replaceChild(elClone, ele)
    }
  })

  // borro eventos no pulsados
  fichasJugador2.forEach(element => {
    if (element !== 'fin') {
      const ele = document.getElementById(element)
      const elClone = ele.cloneNode(true)
      ele.parentNode.replaceChild(elClone, ele)
    }
  })
}

function cambiarTurno () {
  if (turno === 1) {
    turno = 2
    pulsadoTurno1 = false
    setTurno(2)
    // socket.emit('sincronizar', JSON.stringify({ turno: turno, j1: fichasJugador1, j2: fichasJugador2 }))
  } else {
    turno = 1
    pulsadoTurno2 = false
    setTurno(1)
    // socket.emit('sincronizar', JSON.stringify({ turno: turno, j1: fichasJugador1, j2: fichasJugador2 }))
  }
}

socket.on('solicitarDados', function (data) {
  dado1.innerText = data.dados[0]
  dado2.innerText = data.dados[1]
  sacarFichas(Number(dado1.innerText), Number(dado2.innerText))
  borrarEventos()
  if (!movimientosValidos()) {
    alert('No tiene movimientos disponibles, cambio de turno')
    cambiarTurno()
  } else {
    eventosFichas()
  }
  socket.emit('sincronizar', JSON.stringify({ turno: turno, j1: fichasJugador1, j2: fichasJugador2, d1: dado1.innerText, d2: dado2.innerText, b1: bTirar.style.opacity, b2: bTirar2.style.opacity, p1: puntuacion1.innerText, p2: puntuacion2.innerText }))
})

function verificarEventosColocar (fichas) {
  const retorno = []
  fichas.forEach(element => {
    if (!retorno.includes(element)) {
      retorno.push(element)
    }
  })
  return retorno
}

function eventosFichas () {
  if (turno === 1) {
    const fichas = verificarEventosColocar(fichasJugador1)
    for (let index = 0; index < fichas.length; index++) {
      const element = fichas[index]
      if (/* element !== caminoJugador1[0] && */ element !== 'fin' && !element.includes('casa')) {
        colocarEvento(document.getElementById(element))
      }
    }
  } else {
    const fichas = verificarEventosColocar(fichasJugador2)
    for (let index = 0; index < fichas.length; index++) {
      const element = fichas[index]
      if (/* element !== caminoJugador2[0] && */ element !== 'fin' && !element.includes('casa')) {
        colocarEvento(document.getElementById(element))
      }
    }
  }
}

function colocarEvento (ele) {
  ele.addEventListener('click', function mov () {
    if (iniciado) {
      movimiento(turno, Number(dado1.innerText), ele)
      ele.removeEventListener('click', mov)
    } else {
      alert('Esperando oponente')
    }
  })
}

function initBoard () {
  alert(`usted es el jugador ${sessionStorage.getItem('numeroJugador')}`)
  fichasJugador1 = ['casaR1', 'casaR2', 'casaR3', 'casaR4']
  fichasJugador2 = ['casaY1', 'casaY2', 'casaY3', 'casaY4']
  // fichasJugador1 = ['cass44', 'cass45', 'cass46', 'cass47']
  // fichasJugador2 = ['cass43', 'cass42', 'cass41', 'cass40']
  pintarTablero()
}

function setTurno (t) {
  turno = t
  idenTurno.innerText = turno
  if (turno === 1 && !pulsadoTurno1) {
    bTirar2.style.opacity = '0.5'
    bTirar2.style.cursor = 'not-allowed'
    bTirar.style.opacity = '1'
    bTirar.style.cursor = 'pointer'
  } else if (turno === 2 && !pulsadoTurno2) {
    bTirar.style.opacity = '0.5'
    bTirar.style.cursor = 'not-allowed'
    bTirar2.style.opacity = '1'
    bTirar2.style.cursor = 'pointer'
  }
}

function pintarTablero () {
  fichasJugador1.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = 'url("../img/tokenRed.png")'
      ele.style.backgroundSize = (ele.style.backgroundSize === '') ? '100% 100%' : ele.style.backgroundSize
    }
  })

  fichasJugador2.forEach(nombre => {
    if (nombre !== 'fin') {
      const ele = document.getElementById(nombre)
      ele.style.backgroundImage = 'url("../img/tokenAma.png")'
      ele.style.backgroundSize = (ele.style.backgroundSize === '') ? '100% 100%' : ele.style.backgroundSize
    }
  })
  setTurno(turno)
}

initBoard()

function buscarIndice (inicio) {
  if (turno === 1) {
    return fichasJugador1.findIndex(ele => ele === inicio)
  } else {
    return fichasJugador2.findIndex(ele => ele === inicio)
  }
}

function aumentarPuntuacion () {
  if (turno === 1) {
    let punteo = Number(puntuacion1.innerText.substr(3, 1))
    punteo++
    puntuacion1.innerText = ' - ' + punteo
    if (punteo === 4) {
      const data = {
        marcadores: [4, Number(puntuacion2.innerText.substr(3, 1))],
        partida: sessionStorage.getItem('partida')
      }
      socket.emit('gano', data)
    }
  } else {
    let punteo = Number(puntuacion2.innerText.substr(3, 1))
    punteo++
    puntuacion2.innerText = ' - ' + punteo
    if (punteo === 4) {
      const data = {
        marcadores: [puntuacion1.innerText.substr(3, 1), 4],
        partida: sessionStorage.getItem('partida')
      }
      socket.emit('gano', data)
    }
  }
}

function movimiento (jugador, cantidadCasillas, ele) {
  console.log(ele.id + ' click')
  if (jugador !== turno) {
    return 0
  }
  let posicionFinal = ele.id
  const final = turno === 1 ? Number(caminoJugador1[1].substr(4, 2)) : Number(caminoJugador2[1].substr(4, 2))
  let comi = false
  const inicio = ele.id
  if (movimientoValido(inicio, cantidadCasillas)) {
    if (inicio.includes('cassR') || inicio.includes('cassY')) {
      const casillaIni = Number(inicio.substr(5, 2))
      const casillaFin = casillaIni + cantidadCasillas
      posicionFinal = turno === 1 ? 'cassR' : 'cassY'
      if (casillaFin < 6) {
        posicionFinal += casillaFin
        let casilla = document.getElementById(posicionFinal)
        let fotoFinal = '100% 100%'
        if (casilla.style.backgroundSize === '100% 100%') {
          fotoFinal = '50% 100%'
        }
        colocarFicha(posicionFinal, buscarIndice(inicio))
        casilla.style.backgroundSize = fotoFinal
        const urlFoto = turno === 1 ? 'url("../img/tokenRed.png")' : 'url("../img/tokenAma.png")'
        casilla.style.backgroundImage = urlFoto

        casilla = document.getElementById(inicio)
        fotoFinal = ''
        if (casilla.style.backgroundSize === '50% 100%') {
          fotoFinal = '100% 100%'
        }
        casilla.style.backgroundSize = fotoFinal
        casilla.style.backgroundImage = fotoFinal === '' ? '' : urlFoto
      } else {
        colocarFicha('fin', buscarIndice(inicio))
        aumentarPuntuacion()
        let fotoFinal = ''
        const casilla = document.getElementById(inicio)
        if (casilla.style.backgroundSize === '50% 100%') {
          fotoFinal = '100% 100%'
        }
        casilla.style.backgroundSize = fotoFinal
        const urlFoto = turno === 1 ? 'url("../img/tokenRed.png")' : 'url("../img/tokenAma.png")'
        casilla.style.backgroundImage = fotoFinal === '' ? '' : urlFoto
      }
    } else {
      let casillaFin = Number(inicio.substr(4, 2)) + cantidadCasillas
      casillaFin = casillaFin > 52 ? casillaFin - 52 : casillaFin
      posicionFinal = 'cass' + casillaFin
      const fichasJugador = turno === 1 ? fichasJugador2 : fichasJugador1
      const limite = turno === 1 ? 47 : 21
      if (fichasJugador.includes(posicionFinal) /* && antesFinal(Number(inicio.substr(4, 2)), casillaFin) */) { // comer
        let pos = 0
        for (let index = 0; index < fichasJugador.length; index++) {
          const element = fichasJugador[index]
          if (element === posicionFinal) {
            pos = index
            break
          }
        }
        let posFin = turno === 1 ? 'casaY' : 'casaR'
        posFin += (pos + 1)
        // colocarFicha(posFin, pos)
        if (turno === 2) {
          fichasJugador1[pos] = posFin
        } else {
          fichasJugador2[pos] = posFin
        }
        comi = true
        const urlFoto = turno === 1 ? 'url("../img/tokenAma.png")' : 'url("../img/tokenRed.png")'
        const casilla = document.getElementById(posFin)
        casilla.style.backgroundImage = urlFoto
        casilla.style.backgroundSize = '100% 100%'
      }
      if ((final < casillaFin && Number(inicio.substr(4, 2)) <= final) || (cantidadCasillas === 20 && Number(inicio.substr(4, 2)) >= limite)) { // meterlo a la casa
        const casaFin = casillaFin - final
        posicionFinal = turno === 1 ? 'cassR' + casaFin : 'cassY' + casaFin
        let casilla = document.getElementById(posicionFinal)
        const urlFoto = turno === 1 ? 'url("../img/tokenRed.png")' : 'url("../img/tokenAma.png")'
        if (posicionFinal === 'cassR6' || posicionFinal === 'cassY6') {
          colocarFicha('fin', buscarIndice(inicio))
          aumentarPuntuacion()
        } else {
          let fotoFinal = '100% 100%'
          if (casilla.style.backgroundSize === '100% 100%' && !comi) {
            fotoFinal = '50% 100%'
          }
          colocarFicha(posicionFinal, buscarIndice(inicio))
          casilla.style.backgroundSize = fotoFinal
          casilla.style.backgroundImage = urlFoto
        }

        casilla = document.getElementById(inicio)
        let fotoFinal = ''
        if (casilla.style.backgroundSize === '50% 100%') {
          fotoFinal = '100% 100%'
        }
        casilla.style.backgroundSize = fotoFinal
        casilla.style.backgroundImage = fotoFinal === '' ? '' : urlFoto
      } else {
        let casilla = document.getElementById(posicionFinal)
        let fotoFinal = '100% 100%'
        if (casilla.style.backgroundSize === '100% 100%' && !comi) {
          fotoFinal = '50% 100%'
        }
        colocarFicha(posicionFinal, buscarIndice(inicio))
        casilla.style.backgroundSize = fotoFinal
        const urlFoto = turno === 1 ? 'url("../img/tokenRed.png")' : 'url("../img/tokenAma.png")'
        casilla.style.backgroundImage = urlFoto

        casilla = document.getElementById(inicio)
        fotoFinal = ''
        if (casilla.style.backgroundSize === '50% 100%') {
          fotoFinal = '100% 100%'
        }
        casilla.style.backgroundSize = fotoFinal
        casilla.style.backgroundImage = fotoFinal === '' ? '' : urlFoto
      }
    }
    // roto dados
    if (comi) {
      dado1.innerText = 20
    } else {
      dado1.innerText = dado2.innerText
      dado2.innerText = ''
    }
    if (dado1.innerText === '') {
      cambiarTurno()
    } else {
      borrarEventos()
      if (!movimientosValidos()) {
        cambiarTurno()
      } else {
        eventosFichas()
      }
    }
  } else {
    alert('Movimiento Invalido')
    if (!movimientosValidos()) {
      cambiarTurno()
    }
  }
  // quitar evento click
  /* var elClone = ele.cloneNode(true)
  ele.parentNode.replaceChild(elClone, ele) */
  socket.emit('sincronizar', JSON.stringify({ turno: turno, j1: fichasJugador1, j2: fichasJugador2, d1: dado1.innerText, d2: dado2.innerText, b1: bTirar.style.opacity, b2: bTirar2.style.opacity, p1: puntuacion1.innerText, p2: puntuacion2.innerText }))
}

bTirar.addEventListener('click', () => {
  if (iniciado) {
    if (bTirar.style.cursor === 'not-allowed') {
      // nel
    } else {
      if (bTirar.style.cursor !== 'not-allowed' && Number(sessionStorage.getItem('numeroJugador')) === 1) {
        bTirar.style.opacity = '0.5'
        bTirar.style.cursor = 'not-allowed'
        pulsadoTurno1 = true
        socket.emit('solicitarDados', idenTurno.innerText)
      } else if (bTirar.style.cursor === 'not-allowed' && Number(sessionStorage.getItem('numeroJugador')) === 1) {
        alert('termina todos tus movimientos')
      } else {
        alert('No es tu turno, no puedes pulsar este boton')
      }
    }
  } else {
    alert('Esperando oponente')
  }
})

bTirar2.addEventListener('click', () => {
  if (iniciado) {
    if (bTirar2.style.cursor === 'not-allowed') {
      // nel
    } else {
      if (bTirar2.style.cursor !== 'not-allowed' && Number(sessionStorage.getItem('numeroJugador')) === 2) {
        pulsadoTurno2 = true
        socket.emit('solicitarDados', idenTurno.innerText)
        bTirar2.style.opacity = '0.5'
        bTirar2.style.cursor = 'not-allowed'
      } else if (bTirar2.style.cursor === 'not-allowed' && Number(sessionStorage.getItem('numeroJugador')) === 2) {
        alert('termina todos tus movimientos')
      } else {
        alert('No es tu turno, no puedes pulsar este boton')
      }
    }
  } else {
    alert('Esperando oponente')
  }
})
