const socket = io.connect()

function mostrarPartidas () {
  var codigoTabla = '<table border="4"><tr>'
  codigoTabla += '<th>Partida contra el Jugador</th><th>Enlace Partida</th></tr>'
  const data = JSON.parse(sessionStorage.getItem('juegos'))
  for (let index = 0; index < data.length; index++) {
    const objeto = data[index]
    codigoTabla += '<tr>'
    codigoTabla += `<td height="55px">Jugar Contra - ${codigoRival(objeto.jugadores)} </td>`
    codigoTabla += `<td height="55px"><span onclick="redirigir('${objeto.id}')">Jugar Partida</span></td>`
    codigoTabla += '</tr>'
  }

  codigoTabla += '</table>'
  const ms = document.getElementById('meterConsulta')
  ms.innerHTML = codigoTabla
}

function codigoRival (arrayJugadores) {
  const idJugador = sessionStorage.getItem('id')
  const num = arrayJugadores.findIndex(busc => busc.includes(idJugador))
  return num === 0 ? arrayJugadores[1] : arrayJugadores[0]
}

function redirigir (partida) {
  let url = window.location.href
  url = url.substr(0, url.length - 10)
  window.location.replace(`${url}/unirse/${partida}`)
}

mostrarPartidas()
